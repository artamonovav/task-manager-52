package ru.t1.artamonov.tm.api;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getHostName();

    @NotNull
    Integer getPort();

    @NotNull
    String getDbName();

    @NotNull
    String getBrokerUrl();

    @NotNull
    String getBrokerQueue();

}
